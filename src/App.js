
import './App.css';
import ProductCard from './ProductsCard';
import './ProductCard.css';
import Cart from './Cart';
import { useEffect, useState } from 'react';
import useCocktailClientAPI from './ClientAPI/useCocktailClient';
import { CircularProgress } from '@material-ui/core';
import CocktailData from './CocktailData';
import Form from 'react-bootstrap/Form'
import { Button } from 'react-bootstrap';

const App = () => {
  const title = "La mia app in React!"
  const[counter,setCounter] = useState(0)
  // Versione nuova
  const cocktailClient = useCocktailClientAPI()

  console.log(cocktailClient.data)
  const errorObject = {
    message: "Sono un errore"
  }
  // Veccchia 
  const cocktailQuery = {
    isLoading: false,
    isError: false,
    error: errorObject,
    data: ["a"]
  }

  // Dipendenze dello useEffect
  const arrayOfChanges = [] // Vengo eseguito al montaggio del componente (solo una volta)


  useEffect(()=>{
    cocktailClient.fetchDrink("m")
  },arrayOfChanges)

  const product ={
    title: "Secchiello",
    cost: {
      amount: 10,
      currency: "€"
    }
  }
  
  const product2 ={
    title: "Ombrello",
    cost: {
      amount: 6,
      currency: "€"
    }
  }
  const product3 ={
    title: "Tazza",
    cost: {
      amount: 2,
      currency: "€"
    }
  }

  const products =[product,product2,product3]

  const handleAddToCart =()=>{
    setCounter(prev=> {
      const newValue = prev + 1
      if(newValue > 3){
        alert("Hai superato il limite di prodotti")
       return prev
      }
      return newValue
    })
  }
  const handleRemovefromCart =()=>{
    setCounter(prev=>prev-1)
  }

  if (cocktailClient.loading) {
      return (
      <div className="App">
        <header className="App-header white"><CircularProgress /></header>
      </div>
    )
  }

  if (cocktailClient.error) {
    return(
      <div className="App">
        <header  className="App-header white">Error : {cocktailClient.error.message}</header>
      </div>
    )
  }

  if(!cocktailClient.data || cocktailClient.data.length === 0){
    return(
      <div className="App">
        <header className="App-header white">There is no data</header>
      </div>
    )
  }

  return (
    <div className="App">
      <header className="App-header">
        <Cart counter={counter}/>
        <p>{title}</p>
        <div className="container" style={{
          display:"flex"
        }}>
        {/*<p>{product.title}</p>
        <p>Costo: {product.cost.amount}{product.cost.currency}</p>*/}
        {
          products.map((product, index)=>{
            {/*return <div key={index}>
              <p>Prodotto</p>
              <p>{product.title}</p>
              <p>Costo: {product.cost.amount} {product.cost.currency} </p> 
          </div>*/}
            return <ProductCard
              addToCart={handleAddToCart}
              removefromCart ={handleRemovefromCart}
              key={index}
              title={product.title}
              costAmount={product.cost.amount}
              costCurrency={product.cost.currency}
              />
          })
        }
        </div>
        <div className="flex">
          {/* Parte vecchia */}
        {
          cocktailClient.data.map((drink, index)=>{
            return <CocktailData
              key={index}
              drink={drink}
              src={drink.strDrinkThumb}
              />
          })
        }

        {/* Parte nuova */}
        {/*cocktailClient.data.map((drink, index)=>{
          return <div key={index}>
            {drink.strDrink}
            
          </div>
        })*/}
      </div>

      <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control type="email" placeholder="Enter email" />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password" />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicCheckbox">
          <Form.Check type="checkbox" label="Check me out" />
        </Form.Group>
        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form>

        
      </header>
    </div>
  );
}

export default App;
