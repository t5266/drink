import axios from "axios"
import { useState } from "react"

function useCocktailClientAPI(){

    const [loading, setLoading] = useState(true)
    const [data, setData] = useState(undefined)
    const [error, setError] = useState(undefined)

    const fetchDrink = async(name) => {
        try {
            setLoading(true)
            setError(undefined)
            const result = await axios({
                baseURL:'https://www.thecocktaildb.com/api/',
                url:'json/v1/1/search.php',
                method:"GET",
                headers:{
                    Accept: "*/*",
                },
                params:{
                    f:name
                },
            })
            //debugger
           setData(result.data.drinks)
           return result.data.drinks
        } catch (e) {
            console.log(e) 
            setError(e)
            throw e;
        }finally{
            setLoading(false)
        }
    }

    return{
        fetchDrink,
        loading,
        data,
        error
    }
}

export default useCocktailClientAPI