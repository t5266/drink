import { useEffect, useState } from "react"

export default function Cart(props){
    

    const showMessage = () => {
        alert("Hai aggiunto troppi prodotti")
    }

    useEffect(() => {
        if (props.counter >= 2) {
            showMessage()
        }
    },[props.counter])

    return (
        <div><p>Numero di prodotti: {props.counter}</p></div>
    )
}